import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../../utils/common.dart';

final btnSignIn = find.byKey(Key('btnSignIn'));
final fieldUsername = find.byKey(Key('inputUsername'));
final fieldPassword = find.bySemanticsLabel('password');
final customDialog = find.byKey(Key('customDialog'));
final dialogTitle = find.byKey(Key('dialogTitle'));
final dialogDescription = find.byKey(Key('dialogDescription'));
final txtDialogTitle = dialogTitle.evaluate().single.widget as Text;
final txtDialogDescription = dialogDescription.evaluate().single.widget as Text;

Future<void> verifyIsLoginScreen(tester) async {
  await tester.pumpAndSettle();
  expect(btnSignIn, findsOneWidget);
}

Future<void> enterUsername(tester, username) async {
  await tester.pumpAndSettle();
  await tester.enterText(fieldUsername, username);
  await tester.testTextInput.receiveAction(TextInputAction.done);
}

Future<void> enterUsernameV2(tester, username) async {
  await tester.pumpAndSettle();
  await tapAndEnterText(tester, fieldUsername, username);
}

Future<void> enterPassword(tester, password) async {
  await tester.pumpAndSettle();
  await tester.enterText(fieldPassword, password);
  await tester.testTextInput.receiveAction(TextInputAction.done);
}

Future<void> clickSignIn(tester) async {
  await tester.pumpAndSettle();
  await tester.tap(btnSignIn);
}

Future<void> verifyDialog(tester) async {
  await tester.pumpAndSettle();
  expect(customDialog, findsOneWidget);
}

Future<void> verifyDialogTitle(tester, txt) async {
  await tester.pumpAndSettle();
  expect(dialogTitle, findsOneWidget);
  expect(txtDialogTitle.data, txt);
}

Future<void> verifyDialogDescription(tester, txt) async {
  await tester.pumpAndSettle();
  expect(dialogDescription, findsOneWidget);
  expect(txtDialogDescription.data, txt);
}
