import '../screens/home_screen.dart' as homeScreen;
import '../screens/login_screen.dart' as loginScreen;
import '../screens/course_list_screen.dart' as courseListScreen;

Future<void> enterDataAndClickLogin(tester, username, password) async {
  await homeScreen.clickSignInBtn(tester);
  await loginScreen.verifyIsLoginScreen(tester);
  await loginScreen.enterUsername(tester, username);
  await loginScreen.enterPassword(tester, password);
  await loginScreen.clickSignIn(tester);
}

Future<void> verifyLoginSuccess(tester) async {
  await courseListScreen.verifyIsCourseListScreen(tester);
}

Future<void> verifyDialog(tester, title, description) async {
  await loginScreen.verifyDialog(tester);
  await loginScreen.verifyDialogTitle(tester, title);
  await loginScreen.verifyDialogDescription(tester, description);
}
