import 'package:flutter/material.dart';
import 'dart:io';

Future<void> tapAndEnterText(tester, inputText, txt) async {
  if (Platform.isIOS) await tester.tap(inputText);

  await tester.enterText(inputText, txt);

  if (Platform.isAndroid)
    await tester.testTextInput.receiveAction(TextInputAction.done);
}
