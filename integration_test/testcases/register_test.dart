import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:flutter_training/main.dart' as app;
import '../keyword/features/login_feature.dart' as loginFeature;
import '../keyword/features/course_management.dart' as courseManageFeature;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets("TECHQA-005 Register Unix success", (WidgetTester tester) async {
    app.main();
    await loginFeature.enterDataAndClickLogin(tester, 'qa', '112233');
    await loginFeature.verifyLoginSuccess(tester);

    await courseManageFeature.gotoCourseDetail(tester, 'Basic Unix');
    await courseManageFeature.clickRegister(tester);
    await courseManageFeature.enterRegisterInfo(
        tester, 's88225', 'Kanittha Harn', 'email@email.com');
    await courseManageFeature.verifyRegisterCourseSuccess(tester, 'Basic Unix');
    await courseManageFeature.goToAndVerifyCourseRegisterHistory(
        tester, 'Basic Unix');
  }, tags: ['regression']);
}
