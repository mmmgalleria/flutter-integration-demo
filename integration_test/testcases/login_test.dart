import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:flutter_training/main.dart' as app;
import '../keyword/features/login_feature.dart' as loginFeature;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets("QATEST-001 Login Fail", (WidgetTester tester) async {
    app.main();
    await tester.pumpAndSettle();

    final btnSignIn = find.text('Sign in');
    await tester.tap(btnSignIn);

    // verify login input screen
    await tester.pumpAndSettle();
    final btnSignIn2 = find.byKey(Key('btnSignIn'));
    expect(btnSignIn2, findsOneWidget);

    // enter username
    await tester.pumpAndSettle();
    final fieldUsername = find.byKey(Key('inputUsername'));
    await tester.enterText(fieldUsername, 'qa');
    await tester.testTextInput.receiveAction(TextInputAction.done);

    // enter password
    await tester.pumpAndSettle();
    // final fieldPassword = find.byKey(Key('inputPassword'));
    final fieldPassword = find.bySemanticsLabel('password');
    await tester.enterText(fieldPassword, '223344');
    await tester.testTextInput.receiveAction(TextInputAction.done);

    // click sign in
    await tester.pumpAndSettle();
    await tester.tap(btnSignIn2);

    // verify popup
    await tester.pumpAndSettle();
    final txtErrorLoginPopup = find.byKey(Key('customDialog'));
    expect(txtErrorLoginPopup, findsOneWidget);

    final txtErrorLoginTitle = find.byKey(Key('dialogTitle'));
    final dialogTitleTxt = txtErrorLoginTitle.evaluate().single.widget as Text;
    expect(txtErrorLoginTitle, findsOneWidget);
    expect(dialogTitleTxt.data, 'Login Failed');

    final txtErrorLoginTitle2 = find.byWidgetPredicate(
      (Widget widget) => widget is Text && widget.data == 'Login Failed',
      description: 'find text widget with text "Login Failed"',
    );
    expect(txtErrorLoginTitle2, findsOneWidget);

    final dialogTitle3 = find.byWidgetPredicate(
      (Widget widget) =>
          widget is Text &&
          widget.key == Key('dialogTitle') &&
          widget.data == 'Login Failed',
      description: 'find text widget with text "Login Failed"',
    );
    expect(dialogTitle3, findsOneWidget);

    final txtErrorLoginDescription = find.byKey(Key('dialogDescription'));
    expect(txtErrorLoginDescription, findsOneWidget);
    await Future.delayed(Duration(seconds: 1));
  }, tags: ['smoke']);

  testWidgets("QATEST-002 Login Success", (WidgetTester tester) async {
    app.main();
    await tester.pumpAndSettle();

    final btnSignIn = find.text('Sign in');
    await tester.tap(btnSignIn);

    // verify login input screen
    await tester.pumpAndSettle();
    final btnSignIn2 = find.byKey(Key('btnSignIn'));
    expect(btnSignIn2, findsOneWidget);

    // enter username
    await tester.pumpAndSettle();
    final fieldUsername = find.byKey(Key('inputUsername'));
    await tester.enterText(fieldUsername, 'qa');
    await tester.testTextInput.receiveAction(TextInputAction.done);

    // enter password
    await tester.pumpAndSettle();
    // final fieldPassword = find.byKey(Key('inputPassword'));
    final fieldPassword = find.bySemanticsLabel('password');
    await tester.enterText(fieldPassword, '112233');
    await tester.testTextInput.receiveAction(TextInputAction.done);

    // click sign in
    await tester.pumpAndSettle();
    await tester.tap(btnSignIn2);

    // verify success
    await tester.pumpAndSettle();
    final screenTitle = find.text('Training Courses');
    expect(screenTitle, findsOneWidget);
  }, tags: ['regression']);

  testWidgets("QATEST-003 Login Success V2", (WidgetTester tester) async {
    app.main();
    await loginFeature.enterDataAndClickLogin(tester, 'qa', '112233');
    await loginFeature.verifyLoginSuccess(tester);
  }, tags: ['regression']);

  testWidgets("QATEST-004 Login fail V2", (WidgetTester tester) async {
    app.main();
    await loginFeature.enterDataAndClickLogin(tester, 'qa', '223344');
    await loginFeature.verifyDialog(
        tester, 'Login Failed', 'Your user ID or password is incorrect.');
  }, tags: ['regression']);
}
